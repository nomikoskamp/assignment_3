INSERT INTO franchise ( "name" , description) VALUES ('Warner Bros. Pictures','Is a fully integrated, broad-based entertainment company and a global leader in the creation, production, licensing and marketing of all forms of entertainment and their related businesses.');
INSERT INTO franchise ( "name" , description) VALUES ('Columbia Pictures','Columbia Pictures Corporation, Ltd. distributes films. The Company provides the distribution of films, movies, broadcasts, series, and programs for cinema and television to networks, movie theaters, and other clients.');
INSERT INTO franchise ( "name" , description) VALUES ('Marvel','Marvel Comics is an American media and entertainment company regarded as one of the big two publishers in the comics industry. Its parent company, Marvel Entertainment, is a wholly owned subsidiary of the Disney Company. Its headquarters are in New York City.');


INSERT INTO movie( title,  genre, year, director, picture,  trailer,  franchise_id)
VALUES ( 'The Batman', 'Action, Adventure', '2022', 'Matt Reeves',
        'https://upload.wikimedia.org/wikipedia/en/f/ff/The_Batman_%28film%29_poster.jpg',
        'https://www.youtube.com/watch?v=mqqft2x_Aa4', 1);

INSERT INTO movie( title,  genre, year, director, picture,  trailer,  franchise_id)
VALUES ( 'Superman', 'Action, Adventure, Fantasy', '1948', 'Spencer Gordon Bennet, Thomas Carr',
         'https://upload.wikimedia.org/wikipedia/en/thumb/8/81/Superman_serial.jpg/220px-Superman_serial.jpg',
         'https://www.youtube.com/watch?v=YH_5_LFUvNY', 2);

INSERT INTO movie( title,  genre, year, director, picture,  trailer,  franchise_id)
VALUES ( 'Spiderman', 'Action, Adventure, Fantasy', '2002', 'Sam Raimi',
         'https://upload.wikimedia.org/wikipedia/en/thumb/f/f3/Spider-Man2002Poster.jpg/220px-Spider-Man2002Poster.jpg',
         'https://www.youtube.com/watch?v=Ozz8uxW733Q', 3);

INSERT INTO movie( title,  genre, year, director, picture,  trailer,  franchise_id)
VALUES ( 'Avengers', 'Action, Adventure, Fantasy', '2019', 'Anthony Russo, Joe Russo',
         'https://upload.wikimedia.org/wikipedia/en/0/0d/Avengers_Endgame_poster.jpg',
         'https://www.youtube.com/watch?v=TcMBFSGVi1c', 3);

INSERT INTO character("name", alias, gender, picture)
VALUES ('Batman','Bruce Wayne','Male','https://voicefilm.com/wp-content/uploads/2022/04/10-Most-Important-Batman-Nicknames-01.jpg');

INSERT INTO character("name", alias, gender, picture)
VALUES ('Catwoman','Selina','Female','https://static.wikia.nocookie.net/marvel_dc/images/b/b9/Selina_Kyle_Nolanverse_002.jpg/revision/latest?cb=20210524144919');

INSERT INTO character("name", alias, gender, picture)
VALUES ('Superman','Clark','Male','https://upload.wikimedia.org/wikipedia/en/thumb/6/64/Superman_Tyler_Hoechlin.jpg/220px-Superman_Tyler_Hoechlin.jpg');

INSERT INTO character("name", alias, gender, picture)
VALUES ('Spiderman','Little Bastard','Male','https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/spider-man-movies-in-order-index-1631199371.jpeg');

INSERT INTO character("name", alias, gender, picture)
VALUES ('Captain America','CAP','Male','https://cdn.britannica.com/30/182830-050-96F2ED76/Chris-Evans-title-character-Joe-Johnston-Captain.jpg');

INSERT INTO character("name", gender, picture)
VALUES ('Thor','Male','https://upload.wikimedia.org/wikipedia/en/thumb/3/3c/Chris_Hemsworth_as_Thor.jpg/220px-Chris_Hemsworth_as_Thor.jpg');

INSERT INTO character("name", alias, gender, picture)
VALUES ('Black Panther','T Challa','Male','https://www.history.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cfl_progressive%2Cq_auto:good%2Cw_1200/MTU3ODc4NjgzNTEwMTg3NzQz/mcdblpa_ec092-1-2.jpg');

INSERT INTO movie_character (movie_id, character_id) VALUES (1,1);
INSERT INTO movie_character (movie_id, character_id) VALUES (1,2);
INSERT INTO movie_character (movie_id, character_id) VALUES (2,3);
INSERT INTO movie_character (movie_id, character_id) VALUES (3,4);
INSERT INTO movie_character (movie_id, character_id) VALUES (4,5);
INSERT INTO movie_character (movie_id, character_id) VALUES (4,6);
INSERT INTO movie_character (movie_id, character_id) VALUES (4,7);
