package com.example.assignment_3.mappers;

import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.dto.franchise.FranchiseDTO;
import java.util.ArrayList;
import java.util.Collection;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-03T13:32:01+0200",
    comments = "version: 1.5.3.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.6.jar, environment: Java 17.0.6 (Amazon.com Inc.)"
)
@Component
public class FranchiseMapperImpl implements FranchiseMapper {

    @Override
    public FranchiseDTO franchiseToFranchiseDTO(Franchise franchise) {
        if ( franchise == null ) {
            return null;
        }

        FranchiseDTO franchiseDTO = new FranchiseDTO();

        franchiseDTO.setMovies( map( franchise.getMovies() ) );
        franchiseDTO.setId( franchise.getId() );
        franchiseDTO.setName( franchise.getName() );
        franchiseDTO.setDescription( franchise.getDescription() );

        return franchiseDTO;
    }

    @Override
    public Collection<FranchiseDTO> franchiseToFranchiseDTO(Collection<Franchise> franchises) {
        if ( franchises == null ) {
            return null;
        }

        Collection<FranchiseDTO> collection = new ArrayList<FranchiseDTO>( franchises.size() );
        for ( Franchise franchise : franchises ) {
            collection.add( franchiseToFranchiseDTO( franchise ) );
        }

        return collection;
    }
}
