package com.example.assignment_3.mappers;

import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dto.movie.MovieDTO;
import java.util.ArrayList;
import java.util.Collection;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-03T13:32:00+0200",
    comments = "version: 1.5.3.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.6.jar, environment: Java 17.0.6 (Amazon.com Inc.)"
)
@Component
public class MovieMapperImpl implements MovieMapper {

    @Override
    public MovieDTO movieTomovieDTO(Movie movie) {
        if ( movie == null ) {
            return null;
        }

        MovieDTO movieDTO = new MovieDTO();

        movieDTO.setFranchise( movieFranchiseId( movie ) );
        movieDTO.setCharacters( map( movie.getCharacters() ) );
        movieDTO.setId( movie.getId() );
        movieDTO.setTitle( movie.getTitle() );
        movieDTO.setGenre( movie.getGenre() );
        movieDTO.setYear( movie.getYear() );
        movieDTO.setDirector( movie.getDirector() );
        movieDTO.setPicture( movie.getPicture() );
        movieDTO.setTrailer( movie.getTrailer() );

        return movieDTO;
    }

    @Override
    public Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movie) {
        if ( movie == null ) {
            return null;
        }

        Collection<MovieDTO> collection = new ArrayList<MovieDTO>( movie.size() );
        for ( Movie movie1 : movie ) {
            collection.add( movieTomovieDTO( movie1 ) );
        }

        return collection;
    }

    private Integer movieFranchiseId(Movie movie) {
        if ( movie == null ) {
            return null;
        }
        Franchise franchise = movie.getFranchise();
        if ( franchise == null ) {
            return null;
        }
        int id = franchise.getId();
        return id;
    }
}
