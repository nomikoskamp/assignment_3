package com.example.assignment_3.mappers;

import com.example.assignment_3.models.Character;
import com.example.assignment_3.models.dto.character.CharacterDTO;
import java.util.ArrayList;
import java.util.Collection;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-03T13:32:01+0200",
    comments = "version: 1.5.3.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.6.jar, environment: Java 17.0.6 (Amazon.com Inc.)"
)
@Component
public class CharacterMapperImpl implements CharacterMapper {

    @Override
    public CharacterDTO characterToCharacterDTO(Character character) {
        if ( character == null ) {
            return null;
        }

        CharacterDTO characterDTO = new CharacterDTO();

        characterDTO.setMovies( map( character.getMovies() ) );
        characterDTO.setId( character.getId() );
        characterDTO.setName( character.getName() );
        characterDTO.setAlias( character.getAlias() );
        characterDTO.setGender( character.getGender() );
        characterDTO.setPicture( character.getPicture() );

        return characterDTO;
    }

    @Override
    public Collection<CharacterDTO> characterToCharacterDTO(Collection<Character> characters) {
        if ( characters == null ) {
            return null;
        }

        Collection<CharacterDTO> collection = new ArrayList<CharacterDTO>( characters.size() );
        for ( Character character : characters ) {
            collection.add( characterToCharacterDTO( character ) );
        }

        return collection;
    }
}
