# Assignment 3: Create a Web API and database with Spring

## Table of Contents

- [Executive summary](#executive-summary)
- [Technologies](#technologies)
- [General Information](#general-information)
- [Appendix A](#appendix-a)
- [Appendix B](#appendix-b)
- [Swagger](#swagger) 
- [Maintainers](#maintainers)
- [License](#license)

## Executive summary

This is a project which is divided into two main parts:
- [ ] **Appendix A:** Hibernate.
- [ ] **Appendix B:** Web API using Spring Web.

## Technologies

- [ ] Intellij Ultimate
- [ ] Postgres and PgAdmin
- [ ] Java 17
- [ ] Postgres SQL driver, Spring Web, Spring Data JPA, Lombok dependencies
- [ ] Swagger Open API


## General Information

### Appendix A
In this part a PostgreSQL database named MovieDB is created using Hibernate. 
A datastore and interface to store and manipulate movies, movie characters and franchises is created.
The database will store information about characters, movies they appear in, and the franchises 
these movies belong to.

### Appendix B
The task of this part is to be able to do the same manipulations through web. 
This is achieved by using the spring web dependency.

#### Functionality

###### CrudServices
- [ ] `getAll` Reads all the entities in the database.
- [ ] `getById` Reads a specific entity from the database by Id.
- [ ] `add` Add a new entity to the database.
- [ ] `update` Update an existing entity.
- [ ] `delete` Update an existing entity.

###### CustomServices
- [ ] `getMovies` Returns all the belonging movies of a specific franchise by supplied Id.
- [ ] `getCharacters` Returns all the characters included in a movie/franchise by supplied Id.
- [ ] `getFranchise` Returns franchise of a movie by supplied Id.
- [ ] `updateCharacters` Updates the characters of a movie by supplied Id.
- [ ] `updateMovies` Updates the movies of a franchise by supplied Id.


#### Controllers & Mappers

- [ ] `MovieController` & `MovieMapper`
- [ ] `FranchiseController` & `FranchiseMapper`
- [ ] `CharacterController` & `CharacterMapper`


#### Models & DTOs
- [ ] Movie, MovieDTO, MoviePostDTO  
- [ ] Character, CharacterDTO, CharacterSimpleDTO
- [ ] Franchise, FranchiseDTO, FranchiseSimpleDTO

## Swagger
Testing - checking functionality through [Swagger](http://localhost:8080/swagger-ui/index.html) 


## Maintainers

[Nomikos Kampourakis ](https://gitlab.com/nomikoskamp)

[Giannis Tripodis](https://gitlab.com/giatrip)

## License
Giannis Tripodis

Nomikos Kampourakis

@ 2023