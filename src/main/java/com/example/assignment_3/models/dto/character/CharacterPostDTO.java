package com.example.assignment_3.models.dto.character;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CharacterPostDTO {
    private String name;
    private String alias;
    private String gender;
    private String picture;
}
