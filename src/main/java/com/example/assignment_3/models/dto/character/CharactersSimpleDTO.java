package com.example.assignment_3.models.dto.character;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CharactersSimpleDTO {
    private int id;
    private String name;
    private String alias;
    private String gender;
    private String picture;
}
