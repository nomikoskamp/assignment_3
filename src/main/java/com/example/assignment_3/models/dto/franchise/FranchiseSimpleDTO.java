package com.example.assignment_3.models.dto.franchise;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FranchiseSimpleDTO {
    private int id;
    private String name;
    private String description;
}
