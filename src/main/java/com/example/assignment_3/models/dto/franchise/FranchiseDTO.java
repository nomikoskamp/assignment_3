package com.example.assignment_3.models.dto.franchise;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class FranchiseDTO {

    private int id;
    private String name;
    private String description;
    private Set<Integer> movies;
}
