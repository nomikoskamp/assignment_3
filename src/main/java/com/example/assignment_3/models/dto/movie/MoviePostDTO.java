package com.example.assignment_3.models.dto.movie;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoviePostDTO {

    private String title;

    private String genre;

    private String year;

    private String director;

    private String picture;

    private String trailer;
}
