package com.example.assignment_3.models.dto.movie;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MovieDTO {
    private int id;

    private String title;

    private String genre;

    private String year;

    private String director;

    private String picture;

    private String trailer;

    private Integer franchise;

    private Set<Integer> characters;

}
