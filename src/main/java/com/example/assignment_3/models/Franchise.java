package com.example.assignment_3.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)     // Serial id (autoincrement)
    private int id;
    @Column(length = 50, nullable = false)                  // NOT NULL value
    private String name;
    @Column(length = 400)                                                   // TEXT type
    private String description;                             // Might be NULL value
    @JsonIgnore
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}
