package com.example.assignment_3.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)     // Serial id (autoincrement)
    private int id;
    @Column(length = 50, nullable = false)                  // NOT NULL value
    private String name;
    @Column(length = 50)                                    // Might be NULL value
    private String alias;
    @Column(length = 10, nullable = false)                  // NOT NULL value
    private String gender;
    @Column(length = 2083)                                  // TEXT type
    private String picture;                                 // Might be NULL value
    @JsonIgnore
    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;
}
