package com.example.assignment_3.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)     // Serial id (autoincrement)
    private int id;
    @Column(length = 50, nullable = false)                  // NOT NULL value
    private String title;
    @Column(length = 150, nullable = false)                 // NOT NULL value
    private String genre;
    @Column(length = 10, nullable = false)                  // NOT NULL value
    private String year;
    @Column(length = 50, nullable = false)                  // NOT NULL value
    private String director;
    @Column(length = 2083)                                  // TEXT type
    private String picture;                                 // Might be NULL value
    @Column(length = 2083)                                  // TEXT type
    private String trailer;                                 // Might be NULL value
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
    @ManyToMany
    @JoinTable(
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    private Set<Character> characters;


//    @JsonGetter("subjects")
//    public List<String> charactersGetter() {
//        if(characters == null)
//            return null;
//        return characters.stream()
//                .map(s -> "api/v1/subjects/" + s.getId())
//                .collect(Collectors.toList());
//    }
}

