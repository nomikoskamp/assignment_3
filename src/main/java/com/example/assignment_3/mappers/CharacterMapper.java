package com.example.assignment_3.mappers;

import com.example.assignment_3.models.Character;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dto.character.CharacterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface CharacterMapper {
    @Mapping(target = "movies", qualifiedByName = "moviesToMoviesId")
    CharacterDTO characterToCharacterDTO(Character character);

    Collection<CharacterDTO> characterToCharacterDTO(Collection<Character> characters);


    @Named(value = "moviesToMoviesId")
    default Set<Integer> map(Set<Movie> value) {
        if (value == null)
            return null;
        return value.stream()
                .map(s -> s.getId())
                .collect(Collectors.toSet());
    }
}
