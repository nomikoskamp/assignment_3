package com.example.assignment_3.mappers;

import com.example.assignment_3.models.Character;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dto.movie.MovieDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface MovieMapper {

    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", qualifiedByName = "charactersToCharacterId")
    MovieDTO movieTomovieDTO(Movie movie);

    Collection<MovieDTO> movieToMovieDTO(Collection<Movie> movie);


    @Named(value = "charactersToCharacterId")
    default Set<Integer> map(Set<Character> value) {
        if (value == null)
            return null;
        return value.stream()
                .map(s -> s.getId())
                .collect(Collectors.toSet());
    }
}
