package com.example.assignment_3.exceptions;

public class CharacterNotFoundException extends EntityNotFoundException{
    public CharacterNotFoundException(int id) {
        super("Character does not exist with ID: " + id);
    }
}
