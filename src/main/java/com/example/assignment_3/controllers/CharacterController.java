package com.example.assignment_3.controllers;

import com.example.assignment_3.ApiErrorResponse;
import com.example.assignment_3.mappers.CharacterMapper;
import com.example.assignment_3.models.Character;
import com.example.assignment_3.models.dto.character.CharacterDTO;
import com.example.assignment_3.models.dto.character.CharacterPostDTO;
import com.example.assignment_3.models.dto.movie.MovieDTO;
import com.example.assignment_3.services.character.CharacterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    @GetMapping
    @Operation(summary = "Gets all the characters")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Characters found successfully",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))
                    }
            )
    })
    public ResponseEntity findAll() {
        return ResponseEntity.ok(characterMapper.characterToCharacterDTO(characterService.findAll()));
    }

    @GetMapping("{id}")
    @Operation(summary = "Gets a character by its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }) //HERE
    })
    public ResponseEntity findById(@PathVariable int id) {

        return ResponseEntity.ok(characterMapper.characterToCharacterDTO(
                characterService.findById(id)));
    }

    @PostMapping
    @Operation(summary = "Adding a new character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Character added successfully",
                    content = @Content
            )
    })
    public ResponseEntity add(@RequestBody CharacterPostDTO entity) throws URISyntaxException {
        characterService.add(entity);
        URI uri = new URI("api/v1/characters/" + 1);
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("{id}")
    @Operation(summary = "Updates an existing character by its Id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Character with supplied ID updated successfully",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    public ResponseEntity update(@RequestBody Character entity, @PathVariable int id) {
        if (id != entity.getId())
            return ResponseEntity.badRequest().build();
        characterService.update(entity);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Deletes a character by its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Character with supplied ID deleted successfully",
                    content =  @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content =  { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    public ResponseEntity deleteById(@PathVariable int id) {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
