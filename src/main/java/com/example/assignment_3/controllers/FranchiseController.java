package com.example.assignment_3.controllers;

import com.example.assignment_3.ApiErrorResponse;
import com.example.assignment_3.mappers.FranchiseMapper;
import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.dto.character.CharactersSimpleDTO;
import com.example.assignment_3.models.dto.franchise.FranchiseDTO;
import com.example.assignment_3.models.dto.franchise.FranchisePostDTO;
import com.example.assignment_3.models.dto.movie.MovieDTO;
import com.example.assignment_3.services.franchise.FranchiseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
    }

    @GetMapping
    @Operation(summary = "Gets all the franchises")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Franchises found successfully",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))
                    }
            )
    })
    public ResponseEntity findAll() {
        return ResponseEntity.ok(franchiseMapper.franchiseToFranchiseDTO(
                franchiseService.findAll()));
    }

    @GetMapping("{id}")
    @Operation(summary = "Gets a franchise by its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }) //HERE
    })
    public ResponseEntity findById(@PathVariable int id) {

        return ResponseEntity.ok(franchiseMapper.franchiseToFranchiseDTO(
                franchiseService.findById(id)));
    }

    @PostMapping
    @Operation(summary = "Adding a new franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Franchise added successfully",
                    content = @Content
            )
    })
    public ResponseEntity add(@RequestBody FranchisePostDTO entity) throws URISyntaxException {
        franchiseService.add(entity);
        URI uri = new URI("api/v1/franchises/" + 1);
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("{id}")
    @Operation(summary = "Updates an existing franchise by its Id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Franchise with supplied ID updated successfully",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    public ResponseEntity update(@RequestBody Franchise entity, @PathVariable int id) {
        if (id != entity.getId())
            return ResponseEntity.badRequest().build();
        franchiseService.update(entity);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Deletes a franchise by its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise with supplied ID deleted successfully",
                    content =  @Content),
            @ApiResponse(responseCode = "404",
                    description = "v does not exist with supplied ID",
                    content =  { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    public ResponseEntity deleteById(@PathVariable int id) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}/characters")
    @Operation(summary = "Gets all the characters of a franchise by its Id")
    public ResponseEntity getCharacters(@PathVariable int id) {
        return ResponseEntity.ok(franchiseService.getCharacters(id));
    }

    @GetMapping("{id}/movies")
    @Operation(summary = "Gets all the movies of a franchise by its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Characters of the franchise with supplied ID found successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharactersSimpleDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise with supplied ID does not exist ",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    public ResponseEntity getMovies(@PathVariable int id) {
        return ResponseEntity.ok(franchiseService.getMovies(id));
    }

    @PutMapping("{franchiseId}/movies")
    @Operation(summary = "Updates movies in a franchise by their Ids")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Movies of the franchise with supplied ID updated successfully",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    public ResponseEntity updateMovies(@RequestBody int[] movieId, @PathVariable int franchiseId) {
        franchiseService.updateMovies(franchiseId, movieId);
        return ResponseEntity.noContent().build();
    }
}
