package com.example.assignment_3.controllers;

import com.example.assignment_3.ApiErrorResponse;
import com.example.assignment_3.mappers.MovieMapper;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dto.character.CharacterDTO;
import com.example.assignment_3.models.dto.character.CharactersSimpleDTO;
import com.example.assignment_3.models.dto.franchise.FranchiseDTO;
import com.example.assignment_3.models.dto.franchise.FranchiseSimpleDTO;
import com.example.assignment_3.models.dto.movie.MovieDTO;
import com.example.assignment_3.models.dto.movie.MoviePostDTO;
import com.example.assignment_3.services.movie.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "api/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;


    public MovieController(MovieService movieService, MovieMapper movieMapper) {

        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }


    @GetMapping
    @Operation(summary = "Gets all the movies")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Movies found successfully",
                    content = {
                            @Content(mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))
                    }
            )
    })
    public ResponseEntity findAll() {
        return ResponseEntity.ok(
                movieMapper.movieToMovieDTO(
                        movieService.findAll()));
    }

    @GetMapping("{id}")
    @Operation(summary = "Gets a movie by its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) }) //HERE
    })
    public ResponseEntity findById(@PathVariable int id) {
        return ResponseEntity.ok(movieMapper.movieTomovieDTO(
                movieService.findById(id)));
    }

    @PostMapping
    @Operation(summary = "Adding a new movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Movie added successfully",
                    content = @Content
            )
    })
    public ResponseEntity add(@RequestBody MoviePostDTO entity) throws URISyntaxException {
        movieService.add(entity);
        URI uri = new URI("api/v1/movies/" + 1);
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("{id}")
    @Operation(summary = "Updates an existing movie by its Id")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Movie with supplied ID updated successfully",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    public ResponseEntity update(@RequestBody Movie entity, @PathVariable int id) {
        if (id != entity.getId())
            return ResponseEntity.badRequest().build();
        movieService.update(entity);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Deletes a movie by its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie with supplied ID deleted successfully",
                    content =  @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content =  { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    public ResponseEntity deleteById(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    // Get franchise of a movie by id
    @GetMapping("{id}/franchise")
    @Operation(summary = "Gets franchise of a movie by its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Franchise of the movie with supplied found successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseSimpleDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    public ResponseEntity getFranchise(@PathVariable int id) {
        return ResponseEntity.ok(movieService.getFranchise(id));
    }

    // Get characters of a movie by id
    @GetMapping("{id}/characters")
    @Operation(summary = "Gets all the characters of a movie by its Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Characters of the movie with supplied ID found successfully",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharactersSimpleDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Movie with supplied ID does not exist ",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    public ResponseEntity getCharacters(@PathVariable int id) {
        return ResponseEntity.ok(
                movieService.getCharacters(id));
    }

    // Update characters in a movie
    @PutMapping("{movieId}/characters")
    @Operation(summary = "Updates the characters in a movie by their Ids")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Characters of the movie with supplied ID updated successfully",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Bad Request",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))}
            )
    })
    public ResponseEntity updateCharacters(@RequestBody int[] characterId, @PathVariable int movieId) {
        movieService.updateCharacters(movieId, characterId);
        return ResponseEntity.noContent().build();
    }


}
