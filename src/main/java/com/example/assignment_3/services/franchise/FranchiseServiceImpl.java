package com.example.assignment_3.services.franchise;

import com.example.assignment_3.exceptions.FranchiseNotFoundException;
import com.example.assignment_3.exceptions.MovieNotFoundException;
import com.example.assignment_3.models.Character;
import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dto.franchise.FranchisePostDTO;
import com.example.assignment_3.repositories.FranchiseRepository;
import com.example.assignment_3.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class FranchiseServiceImpl implements FranchiseService {

    private final FranchiseRepository franchiseRep;
    private final MovieRepository movieRep;

    public FranchiseServiceImpl(FranchiseRepository franchiseRep, MovieRepository movieRep) {
        this.franchiseRep = franchiseRep;
        this.movieRep = movieRep;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRep.findById(id).orElseThrow(()                         // If there is not a franchise with that id
                -> new FranchiseNotFoundException(id));                         // throw not found exception
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRep.findAll();
    }

    @Override
    public Franchise add(FranchisePostDTO entity) {
        Franchise f = new Franchise();
        f.setName(entity.getName());
        f.setDescription(entity.getDescription());
        return franchiseRep.save(f);
    }

    @Override
    public Franchise update(Franchise entity) {
        if(franchiseRep.existsById(entity.getId())){
            return franchiseRep.save(entity);
        } else
            throw new FranchiseNotFoundException(entity.getId());
    }

    @Override
    public void deleteById(Integer id) {
        if(franchiseRep.existsById(id)){                                // Checks if the franchise with supplied id exists
            List<Movie> movies = movieRep.findAll().stream().toList();  // Stores all Movies to a Movie List
            for (Movie m: movies) {                                     // Checks if each movie's franchise is equal
                if(m.getFranchise().getId() == id){                     // to the franchise we are to delete
                    m.setFranchise(null);                               // and setting it to null
                }
                movieRep.save(m);
            }
            franchiseRep.deleteById(id);
        }                                                                // Then deletes the franchise
        else
            throw new FranchiseNotFoundException(id);                    // If the franchise with supplied id does not exist
    }                                                                    // throws exception

    @Override
    public boolean exists(Integer id) {
        return franchiseRep.existsById(id);
    }

    @Override
    public Collection<Movie> getMovies(int franchiseId) {

        return franchiseRep.findById(franchiseId).orElseThrow(()                    // If there is not a franchise with that id
                -> new FranchiseNotFoundException(franchiseId)).getMovies();        // throw not found exception and get its movies
    }

    @Override
    public Collection<Character> getCharacters(int franchiseId) {
        Collection<Movie> movies = franchiseRep.findById(franchiseId).orElseThrow(()    // If there is not a franchise with that id
                -> new FranchiseNotFoundException(franchiseId)).getMovies();            // throw not found exception and get its movies
        Set<Character> characterList = new HashSet<>();
        for (Movie m : movies) {                                                        // For every movie, finds its characters
            characterList.addAll(m.getCharacters());                                    // Adding those characters into a list
        }
        return characterList;
    }

    @Override
    public void updateMovies(int franchiseId, int[] movieId) {
        Franchise franchise = franchiseRep.findById(franchiseId).orElseThrow(()         // If there is not a franchise with that id
                -> new FranchiseNotFoundException(franchiseId));                        // throw not found exception
        for (int id : movieId) {                                                        // For every id (of movies)
            Movie movie = movieRep.findById(id).orElseThrow(()                          // Finds the movie by its id
                    -> new MovieNotFoundException(id));                                 // throw not found exception, if there are no movies with that id
            movie.setFranchise(franchise);                                              // sets franchise to the movie
            movieRep.save(movie);
        }
    }

}
