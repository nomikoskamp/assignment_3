package com.example.assignment_3.services.franchise;

import com.example.assignment_3.models.Character;
import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dto.franchise.FranchisePostDTO;
import com.example.assignment_3.repositories.FranchiseRepository;
import com.example.assignment_3.services.CrudService;

import javax.xml.stream.events.Characters;
import java.util.Collection;

public interface FranchiseService extends CrudService<Franchise, FranchisePostDTO, Integer> {
    Collection<Movie> getMovies(int franchiseId);

    Collection<Character> getCharacters(int franchiseId);

    void updateMovies(int franchiseId, int[] movieId);



}
