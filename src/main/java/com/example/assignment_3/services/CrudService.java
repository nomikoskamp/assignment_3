package com.example.assignment_3.services;

import java.util.Collection;

public interface CrudService<T, K, ID> {
    T findById(ID id);

    Collection<T> findAll();

    T add(K entity);

    T update(T entity);

    void deleteById(ID id);

    boolean exists(ID id);
}

