package com.example.assignment_3.services.character;

import com.example.assignment_3.models.Character;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dto.character.CharacterPostDTO;
import com.example.assignment_3.services.CrudService;

import java.util.Collection;

public interface CharacterService extends CrudService<Character, CharacterPostDTO, Integer> {

}
