package com.example.assignment_3.services.character;

import com.example.assignment_3.exceptions.CharacterNotFoundException;
import com.example.assignment_3.exceptions.FranchiseNotFoundException;
import com.example.assignment_3.models.Character;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dto.character.CharacterPostDTO;
import com.example.assignment_3.repositories.CharacterRepository;
import com.example.assignment_3.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRep;
    private final MovieRepository movieRep;

    public CharacterServiceImpl(CharacterRepository characterRep, MovieRepository movieRep) {
        this.characterRep = characterRep;
        this.movieRep = movieRep;
    }

    @Override
    public Character findById(Integer id) {
        return characterRep.findById(id).orElseThrow(()                             // If there is not a character with that id
                -> new CharacterNotFoundException(id));                             // throw not found exception
    }

    @Override
    public Collection<Character> findAll() {
        return characterRep.findAll();
    }

    @Override
    public Character add(CharacterPostDTO entity) {
        Character c = new Character();
        c.setAlias(entity.getAlias());
        c.setName(entity.getName());
        c.setPicture(entity.getPicture());
        c.setGender(entity.getGender());
        return characterRep.save(c);
    }

    @Override
    public Character update(Character entity) {
        if(characterRep.existsById(entity.getId())){
            return characterRep.save(entity);
        } else
            throw new CharacterNotFoundException(entity.getId());
    }

    @Override
    public void deleteById(Integer id) {
            if(characterRep.existsById(id)){                                // Checks if the Character with supplied id exists
                List<Movie> movies = movieRep.findAll().stream().toList();  // Stores all Movies to a Movie List
                for (Movie m: movies) {
                    List<Character> characters = m.getCharacters().stream().toList(); // Stores all the Characters in a specific movie, into a Character list
                    Set<Character> newCharacters = new HashSet<>();
                    for(Character c: characters) {                          // For each character in a specific movie
                        if (c.getId() == id) {                              // Checks if each character's id is equal to movie's character id
                            c.setMovies(null);                              // and setting it to null
                        }
                        else
                            newCharacters.add(c);
                        characterRep.save(c);
                    }
                    m.setCharacters(newCharacters);

                }
                characterRep.deleteById(id);}                                // Then deletes the movie
            else
                throw new FranchiseNotFoundException(id);                    // If the movie with supplied id does not exist
    }                                                                        // throws an exception

    @Override
    public boolean exists(Integer id) {
        return characterRep.existsById(id);
    }


}
