package com.example.assignment_3.services.movie;

import com.example.assignment_3.exceptions.CharacterNotFoundException;
import com.example.assignment_3.exceptions.FranchiseNotFoundException;
import com.example.assignment_3.exceptions.MovieNotFoundException;
import com.example.assignment_3.models.Character;
import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dto.movie.MoviePostDTO;
import com.example.assignment_3.repositories.CharacterRepository;
import com.example.assignment_3.repositories.FranchiseRepository;
import com.example.assignment_3.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRep;

    private final CharacterRepository characterRep;
    private final FranchiseRepository franchiseRep;

    public MovieServiceImpl(MovieRepository movieRep, CharacterRepository characterRep, FranchiseRepository franchiseRep) {
        this.movieRep = movieRep;
        this.characterRep = characterRep;
        this.franchiseRep = franchiseRep;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRep.findById(id).orElseThrow(()                     // If there is not a movie with that id
                -> new MovieNotFoundException(id));                     // throw not found exception
    }


    @Override
    public Collection<Movie> findAll() {
        return movieRep.findAll();
    }

    @Override
    public Movie add(MoviePostDTO entity) {
        Movie m = new Movie();                                           // Adding fields of MoviePostDTO
        m.setTitle(entity.getTitle());                                   // to a new Movie's fields
        m.setYear(entity.getYear());                                     // setting Franchise and Characters to null
        m.setGenre(entity.getGenre());                                   // at the init creation of a movie
        m.setDirector(entity.getDirector());
        m.setPicture(entity.getPicture());
        m.setTrailer(entity.getTrailer());
        return movieRep.save(m);
    }

    @Override
    public Movie update(Movie entity) {
        if(movieRep.existsById(entity.getId())){
            return movieRep.save(entity);
        } else
            throw new MovieNotFoundException(entity.getId());
    }

    @Override
    public void deleteById(Integer id) {
        int franchiseId = movieRep.findById(id).get().getFranchise().getId();
        if (movieRep.existsById(id)) {
            franchiseRep.findById(franchiseId).get().setMovies(null);
            movieRep.deleteById(id);
        } else
            throw new MovieNotFoundException(id);
    }

    @Override
    public boolean exists(Integer id) {
        return movieRep.existsById(id);
    }

    @Override
    public Collection<Character> getCharacters(int movieId) {

        return movieRep.findById(movieId).orElseThrow(()                        // If there is not a movie with that id
                -> new MovieNotFoundException(movieId)).getCharacters();        // throw not found exception else get the characters of the movie
    }

    @Override
    public Franchise getFranchise(int movieId) {

        return movieRep.findById(movieId).orElseThrow(()                         // If there is not a movie with that id
                -> new MovieNotFoundException(movieId)).getFranchise();          // throw not found exception else get the franchise of the movie
    }

    @Override
    public void updateCharacters(int movieId, int[] characterId) {
        Movie movie = movieRep.findById(movieId).orElseThrow(()                   // Finds the movie with a specific id
                -> new MovieNotFoundException(movieId));                          // throw not found exception if there isn't any movie
        Set<Character> characterList = new HashSet<>();

        for (int id : characterId) {
            characterList.add(characterRep.findById(id).orElseThrow(()             // Finds the characters of the movie by their id, add them to a list
                    -> new CharacterNotFoundException(movieId)));                  // throw not found exception if there isn't any movie
        }
        movie.setCharacters(characterList);                                        // Add characters to the movie
        movieRep.save(movie);
    }

}
