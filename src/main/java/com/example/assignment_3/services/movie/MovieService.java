package com.example.assignment_3.services.movie;

import com.example.assignment_3.models.Character;
import com.example.assignment_3.models.Franchise;
import com.example.assignment_3.models.Movie;
import com.example.assignment_3.models.dto.movie.MoviePostDTO;
import com.example.assignment_3.services.CrudService;

import java.util.Collection;

public interface MovieService extends CrudService<Movie,MoviePostDTO, Integer> {
    Collection<Character> getCharacters(int movieId);


    Franchise getFranchise(int movieId);

    void updateCharacters(int movieId, int[] characterId);



}
