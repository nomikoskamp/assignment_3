package com.example.assignment_3.repositories;

import com.example.assignment_3.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {

}
