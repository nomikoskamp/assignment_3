package com.example.assignment_3;

import lombok.Data;

@Data
public class ApiErrorResponse {
        private String timestamp;
        private Integer status;
        private String error;
        private String path;

}
